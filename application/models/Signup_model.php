<?php

class Signup_model extends CI_Model{
    function check_email($email){
		$this->db->where('email', $email);
		$query = $this->db->get('emails');
		return ($query->num_rows() > 0) ? TRUE : FALSE;
    }

    function add_email(){
        $data = array(
            'email' => $this->input->post('email')
         );
        $this->db->insert('emails', $data);
    }
}