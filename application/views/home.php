
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>LibroNow - Spend less. Read More</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="all" />
      <link rel="stylesheet" href="assets/css/font-awesome.css" media="all" />
      <link rel="stylesheet" href="assets/css/themify-icons.css" media="all" />
      <link rel="stylesheet" href="assets/css/superfish.css" media="all" />
      <link rel="stylesheet" href="assets/css/megafish.css" media="all" />
      <link rel="stylesheet" href="assets/css/jquery.navgoco.css" media="all" />
      <link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.css" media="all" />
      <link rel="stylesheet" href="assets/css/owl.carousel.css" media="all" />
      <link rel="stylesheet" href="assets/css/owl.theme.css" media="all" />
      <link rel="stylesheet" href="assets/css/animate.css" media="all" />
      <link rel="stylesheet" href="assets/style.css" media="all" >
      <script src="assets/js/modernizr.custom.js"></script>
      <!-- Le fav and touch icons -->
      <link rel="shortcut icon" href="assets/imgs/favicon.ico">
      <link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon.png">
      <link rel="apple-touch-icon" sizes="72x72" href="assets/img/icons/apple-touch-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="114x114" href="assets/img/icons/apple-touch-icon-114x114.png">
    </head>
<body class="">
    <section class="kopa-area kopa-area-23 kopa-area-parallax text-center">
        <div class="widget reading-module-cs">
            <div class="container">
                <!-- logo -->
                <div class="kopa-logo">
                    <a href="http://libronow.com/">
                        <img src="assets/imgs/Libro-NoBG.png" alt="LibroNow Logo" width="400">
                    </a>
                </div>
                <header class="widget-header style-01">
                    <p>Looking for a specific book? Destashing old books? We got you covered.</p>
                </header>
            </div>
            <!-- container -->
            <div class="ct-countdown-4 white-text-style" data-year="2017" data-month="5" data-day="3" data-time="12:00:00">
                <div class="container">
                    <ul></ul>
                </div>
                <!-- container -->
            </div>
            <!-- ct-countdown-2 -->
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12 col-md-push-2">
                    <div class="widget-content-area-2">
                        <h1>Join our mailing list to know more!</h1>
                    </div>
                    <header class="widget-header style-01">
                        <p>We'll keep you posted. You can also check out our Facebook page for updates.</p>
                    </header>
                        <form class="newsletter-form" id="signUp">
                            <input type="text" size="40" class="email" name="email" placeholder="Enter your e-mail here" >
                            <button type="submit" class="submit">
                                <i class="fa fa-paper-plane"></i>
                            </button>
                        </form>
                    </div>
                    <!-- col-md-8 -->
                </div>
                <!-- row -->
                <div class="clearfix"></div>
                <div class="kopa-social-links style-02">
                    <ul class="clearfix">
                        <li><a href="https://www.facebook.com/LibroNowPH/" target="_blank" class="fa fa-facebook"></a></li>
                        <!-- <li><a href="#" class="fa fa-twitter"></a></li> -->
                    </ul>
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- widget -->
        <p class="copyright">&copy; Copyright 2017. Powered by <a href="https://www.facebook.com/tweebsandcodes/" target="_blank">Tweebs&Codes</a></p>
    </section>
    <!-- kopa-area-23 -->
<script src="assets/js/jquery-1.12.4.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/superfish.min.js"></script>
<script src="assets/js/jquery.navgoco.min.js"></script>
<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/js/jquery.mousewheel.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script src="assets/js/masonry.pkgd.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/jquery.sliderPro.min.js"></script>
<script src="assets/js/jquery.validate.min.js"></script>
<script src="assets/js/jquery.countdown.min.js"></script>
<script src="assets/js/jquery.matchHeight-min.js"></script>
<script src="assets/js/jquery.wow.js"></script>
<script src="assets/js/custom.js" charset="utf-8"></script>
<script src="assets/js/libro.js" charset="utf-8"></script>
</body>
</html>
