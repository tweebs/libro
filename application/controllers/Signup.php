<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct(){
		parent::__construct();
		$this->load->model('Signup_model', 'signup');
    }

	public function index(){
		$email = $this->input->post('email');
		//EMAIL
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|callback_check_email',array(
            'required' => 'Email address is required!',
            'valid_email' => 'You entered an invalid email address!',
            'check_email' => sprintf("%s is already registered!", $email)
        ));

		if ($this->form_validation->run() == FALSE) {
			$data['isValid'] = false;
			$return_errors = $this->form_validation->error_array();
            if ($return_errors) {
                foreach ($return_errors as $key => $value) {
                    $field_name[] = $key;
                    $error_message[] = $value;
                }
            }
            $data['field_name'] = $field_name;
            $data['error_message'] = $error_message;
            echo json_encode($data);
		}else{
			$this->signup->add_email();
			$data['isValid'] = true;
			echo json_encode($data);
		}
	}

	public function check_email() {
		$email = $this->input->post('email');
        $checked_email = $this->signup->check_email($email);
        return $checked_email;
    }

}
